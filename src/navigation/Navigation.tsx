import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import BasketScreen from '../screens/BasketScreen.tsx';
import ProfileScreen from '../screens/ProfileScreen..tsx';
import ListScreen from '../screens/ListScreen.tsx';
import ProfileEditButton from '../screens/components/ProfileEditButton.tsx';
import OrderHistoryScreen from '../screens/OrderHistoryScreen.tsx';
import {RootStackParamList} from '../inerfaces/interfaces.ts';
import {
  BASKET_SCREEN,
  LIST_SCREEN,
  ORDER_HISTORY_SCREEN,
  PROFILE_SCREEN,
} from '../consts/consts.ts';

const Stack = createNativeStackNavigator<RootStackParamList>();
const renderProfileEditButton = () => {
  return <ProfileEditButton />;
};
const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={LIST_SCREEN}>
        <Stack.Screen
          name={LIST_SCREEN}
          component={ListScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name={PROFILE_SCREEN}
          component={ProfileScreen}
          options={{
            title: 'Profile',
            headerRight: renderProfileEditButton,
          }}
        />
        <Stack.Screen
          name={ORDER_HISTORY_SCREEN}
          component={OrderHistoryScreen}
          options={{
            title: 'Order History',
          }}
        />
        <Stack.Screen
          name={BASKET_SCREEN}
          component={BasketScreen}
          options={{title: 'Shopping cart'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
