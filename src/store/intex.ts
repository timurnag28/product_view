import productsStore from './ProductsStore.ts';
import profileStore from './ProfileStore.ts';

const store = {
  productsStore,
  profileStore,
};
export default store;
