import {makeAutoObservable} from 'mobx';
import data from './../consts/products.json';
import {OrderHistoryList, ProductItem} from '../inerfaces/interfaces.ts';
import {Alert} from 'react-native';

class ProductsStore {
  constructor() {
    makeAutoObservable(this);
  }

  productList: ProductItem[] = data.products;
  filteredProductList: ProductItem[] = [];
  basketProductList: ProductItem[] = [];
  currentProduct: ProductItem | null = null;
  orderHistoryList: OrderHistoryList[] = [];

  totalBasketPrice: number = 0;
  amountOfProductsInBasket: number = 0;
  searchText: string = '';
  isBottomSheetActive: boolean = false;

  onSetSearchText = (text: string) => {
    this.searchText = text;
  };

  onCancelSearch = () => {
    this.searchText = '';
  };

  onSearchProduct = (searchableText: string) => {
    if (searchableText) {
      this.filteredProductList = this.productList.filter(productItem =>
        productItem.title.toLowerCase().includes(searchableText.toLowerCase()),
      );
    } else {
      this.filteredProductList = this.productList;
    }
  };

  onSetCurrentProduct = (item: ProductItem) => {
    this.currentProduct = item;
  };

  setBottomSheetActive = (value: boolean) => {
    this.isBottomSheetActive = value;
  };

  addToBasket = (productItem: ProductItem) => {
    const productIndex = this.basketProductList.findIndex(
      item => item.id === productItem.id,
    );
    if (productIndex !== -1) {
      this.basketProductList[productIndex].amount! += 1;
    } else {
      this.basketProductList.push({...productItem, amount: 1});
    }
    this.totalBasketPrice += parseFloat(productItem.price);
    this.amountOfProductsInBasket += 1;
  };

  removeFromBasket(productItem: ProductItem) {
    const productIndex = this.basketProductList.findIndex(
      item => item.id === productItem.id,
    );
    if (productIndex !== -1) {
      if (this.basketProductList[productIndex].amount! > 1) {
        this.basketProductList[productIndex].amount! -= 1;
      } else {
        this.basketProductList.splice(productIndex, 1);
      }
      this.totalBasketPrice -= parseFloat(productItem.price);
      this.amountOfProductsInBasket -= 1;
    }
  }

  onOrderProducts = () => {
    if (this.orderHistoryList.length === 0) {
      this.orderHistoryList.push({
        orderId: 1,
        totalPrice: this.totalBasketPrice,
        products: [...this.basketProductList],
      });
    } else {
      let lastElement = this.orderHistoryList[this.orderHistoryList.length - 1];
      this.orderHistoryList.push({
        orderId: lastElement.orderId + 1,
        totalPrice: this.totalBasketPrice,
        products: [...this.basketProductList],
      });
    }

    this.onClearBasket();
    Alert.alert('You have successfully placed your order!');
  };

  onClearBasket = () => {
    this.basketProductList = [];
    this.totalBasketPrice = 0;
    this.amountOfProductsInBasket = 0;
  };
}

const productsStore = new ProductsStore();
export default productsStore;
