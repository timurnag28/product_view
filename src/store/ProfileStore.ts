import {makeAutoObservable} from 'mobx';

class ProfileStore {
  constructor() {
    makeAutoObservable(this);
  }

  userName: string = 'Sasha Phillips';
  temporaryName: string = 'Sasha Phillips';
  isEditMode: boolean = false;

  setUserName = (text: string) => {
    this.temporaryName = text;
  };

  setEditMode = (value: boolean) => {
    this.isEditMode = value;
  };

  onSaveChanges = (onSaveChanges: boolean) => {
    onSaveChanges
      ? (this.userName = this.temporaryName)
      : (this.temporaryName = this.userName);
  };
}

const profileStore = new ProfileStore();
export default profileStore;
