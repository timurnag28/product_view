import {SharedValue} from 'react-native-reanimated';

export interface ProductItem {
  id: number;
  title: string;
  product_type: string;
  price: string;
  image: string | null;
  amount?: number;
}

export interface OrderHistoryList {
  orderId: number;
  totalPrice: number;
  products: ProductItem[];
}

export interface ProductListProps {
  toggleSheet: (value: boolean, isAnimation?: boolean) => void;
}

export interface BottomSheetProps {
  toggleSheet: (value: boolean) => void;
  offset: SharedValue<number>;
  height: SharedValue<number>;
}

export interface HeaderProps {
  toggleSheet: (value: boolean, isAnimation?: boolean) => void;
}

export type RootStackParamList = {
  ListScreen: undefined;
  ProfileScreen: undefined;
  OrderHistoryScreen: {products: ProductItem[]};
  BasketScreen: undefined;
};
