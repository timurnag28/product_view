import {observer} from 'mobx-react-lite';
import store from '../../store/intex.ts';
import React, {useEffect} from 'react';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome5';
import {ProductItem, ProductListProps} from '../../inerfaces/interfaces.ts';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  imageSize,
  size14,
  size16,
  size22,
  WINDOW_HEIGHT,
} from '../../consts/consts.ts';

const ProductList = ({toggleSheet}: ProductListProps) => {
  const {
    filteredProductList,
    searchText,
    isBottomSheetActive,
    onSearchProduct,
  } = store.productsStore;

  useEffect(() => {
    const timeOut = setTimeout(() => onSearchProduct(searchText), 500);
    return () => clearTimeout(timeOut);
  }, [searchText]);

  return (
    <FlatList
      style={style.listContainer}
      contentContainerStyle={
        isBottomSheetActive
          ? style.contentContainerWithSheetStyle
          : style.contentContainerStyle
      }
      data={filteredProductList}
      renderItem={({item}) => renderItem(item, toggleSheet)}
      keyExtractor={item => item.id.toString()}
    />
  );
};

const renderItem = (
  item: ProductItem,
  toggleSheet: (value: boolean) => void,
) => {
  return (
    <TouchableOpacity
      onPress={() => {
        if (
          item.id === store.productsStore.currentProduct?.id &&
          store.productsStore.isBottomSheetActive
        ) {
          toggleSheet(false);
        } else {
          store.productsStore.onSetCurrentProduct(item);
          toggleSheet(true);
        }
      }}
      style={style.itemContainer}>
      <View>
        {item.image ? (
          <Image
            style={style.image}
            source={{
              uri: item.image,
            }}
            resizeMode={'contain'}
          />
        ) : (
          <View style={style.image}>
            <Text>No image</Text>
          </View>
        )}
      </View>
      <View style={style.infoBlock}>
        <View>
          <Text style={style.titleText}>{item.title}</Text>
          <Text style={style.priceText}>{`${item.price} ₽.`}</Text>
        </View>

        <TouchableOpacity
          style={style.basket}
          onPress={() => store.productsStore.addToBasket(item)}>
          <Text style={style.basketText}>Add to cart</Text>
          <IconFontAwesome
            style={style.iconBasket}
            name={'shopping-basket'}
            size={size16}
            color="#fff"
          />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
};

const style = StyleSheet.create({
  listContainer: {
    height: WINDOW_HEIGHT,
    margin: 6,
    flex: 1,
  },
  contentContainerWithSheetStyle: {
    paddingBottom: WINDOW_HEIGHT / 4,
  },
  contentContainerStyle: {paddingBottom: 10},
  itemContainer: {
    backgroundColor: '#fff',
    borderRadius: 12,
    flexDirection: 'row',
    paddingHorizontal: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 12,
    marginHorizontal: 12,
  },
  image: {
    height: imageSize,
    width: imageSize,
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoBlock: {
    marginLeft: 12,
    marginVertical: size22,
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    flex: 1,
  },
  basket: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#7163ed',
    borderRadius: 20,
    paddingVertical: 8,
  },
  iconBasket: {paddingLeft: 6},
  basketText: {fontSize: size14, color: '#fff'},
  titleText: {fontWeight: 'bold', fontSize: size16, color: '#000'},
  priceText: {
    fontWeight: 'bold',
    fontSize: size14,
    color: '#7464ef',
    paddingTop: 2,
  },
});

export default observer(ProductList);
