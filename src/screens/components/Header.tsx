import React from 'react';
import store from '../../store/intex.ts';
import {observer} from 'mobx-react-lite';
import {HeaderProps} from '../../inerfaces/interfaces.ts';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome5';
import {
  NavigationProp,
  ParamListBase,
  useNavigation,
} from '@react-navigation/native';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  BASKET_SCREEN,
  PROFILE_SCREEN,
  size12,
  size14,
  size20,
  size24,
  size44,
  WINDOW_HEIGHT,
  WINDOW_WIDTH,
} from '../../consts/consts.ts';

const Header = ({toggleSheet}: HeaderProps) => {
  const navigation: NavigationProp<ParamListBase> = useNavigation();
  const {searchText, amountOfProductsInBasket, onSetSearchText} =
    store.productsStore;
  return (
    <View style={style.container}>
      <TouchableOpacity
        onPress={() => navigation.navigate(PROFILE_SCREEN)}
        style={style.profileIcon}>
        <IconIonicons name={'person-sharp'} size={size24} color="#000" />
      </TouchableOpacity>

      <TextInput
        onFocus={() => toggleSheet(false, false)}
        style={style.searchInput}
        placeholder="Search"
        value={searchText}
        onChangeText={onSetSearchText}
      />
      <TouchableOpacity
        onPress={() => navigation.navigate(BASKET_SCREEN)}
        style={style.cartIconContainer}>
        <IconFontAwesome
          style={style.cartIcon}
          name={'shopping-cart'}
          size={size24}
          color="#000"
        />
        {amountOfProductsInBasket !== 0 && (
          <View style={style.amountCircle}>
            <Text style={style.amountText}>{amountOfProductsInBasket}</Text>
          </View>
        )}
      </TouchableOpacity>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    height: WINDOW_HEIGHT / 12,
    width: WINDOW_WIDTH,
    backgroundColor: '#fff',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  searchInput: {
    width: WINDOW_WIDTH / 2,
    height: WINDOW_WIDTH / 9,
    backgroundColor: '#f4f5f7',
    borderRadius: 16,
    paddingLeft: 10,
  },
  profileIcon: {padding: 12},
  cartIconContainer: {padding: 12},
  cartIcon: {paddingLeft: 6},
  amountCircle: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    backgroundColor: '#7163ed',
    height: size24,
    width: size24,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  amountText: {color: '#fff', fontSize: size12},
});

export default observer(Header);
