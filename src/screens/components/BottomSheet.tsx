import React from 'react';
import {Text} from 'react-native';
import {StyleSheet} from 'react-native';
import {Gesture, GestureDetector} from 'react-native-gesture-handler';
import {size18, WINDOW_HEIGHT, WINDOW_WIDTH} from '../../consts/consts';
import {BottomSheetProps} from '../../inerfaces/interfaces.ts';
import store from '../../store/intex.ts';
import {observer} from 'mobx-react-lite';
import Animated, {
  runOnJS,
  useAnimatedStyle,
  withSpring,
} from 'react-native-reanimated';

const BottomSheet = ({toggleSheet, offset, height}: BottomSheetProps) => {
  const translateY = useAnimatedStyle(() => ({
    transform: [{translateY: offset.value}],
  }));

  const pan = Gesture.Pan()
    .onChange(event => {
      const offsetDelta = event.changeY + offset.value;
      const clamp = Math.max(-10, offsetDelta);
      offset.value = offsetDelta > 0 ? offsetDelta : withSpring(clamp);
    })
    .onFinalize(() => {
      if (offset.value < WINDOW_HEIGHT / 6) {
        offset.value = withSpring(0);
      } else {
        runOnJS(toggleSheet)(false);
      }
    });
  return (
    <GestureDetector gesture={pan}>
      <Animated.View style={[styles.box, translateY, {height: height}]}>
        <Text style={styles.detailsText}>
          {store.productsStore.currentProduct?.id}
        </Text>
        <Text style={styles.detailsText}>
          {store.productsStore.currentProduct?.title}
        </Text>
        <Text style={styles.detailsText}>
          {store.productsStore.currentProduct?.price}
        </Text>
        <Text style={styles.detailsText}>
          {store.productsStore.currentProduct?.product_type}
        </Text>
      </Animated.View>
    </GestureDetector>
  );
};

const styles = StyleSheet.create({
  box: {
    paddingHorizontal: 24,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    width: WINDOW_WIDTH,
    position: 'absolute',
    bottom: 0,
    zIndex: 1,
    borderWidth: 1,
    borderColor: 'grey',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.1,
    shadowRadius: 24,
    elevation: 10,
  },
  detailsText: {
    fontSize: size18,
    color: '#000',
  },
});

export default observer(BottomSheet);
