import React from 'react';
import {StyleSheet, View} from 'react-native';
import {observer} from 'mobx-react-lite';
import store from '../../store/intex.ts';
import {size36} from '../../consts/consts.ts';
import IconMaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';

const ProfileEditButton = () => {
  const {isEditMode, setEditMode, onSaveChanges} = store.profileStore;
  return (
    <View>
      <IconMaterialCommunity
        onPress={() => {
          console.log('555');
          setEditMode(!isEditMode);
          onSaveChanges(true);
        }}
        style={style.editIconStyle}
        name={isEditMode ? 'check' : 'account-edit'}
        size={size36}
        color="#000"
      />
    </View>
  );
};

const style = StyleSheet.create({
  editIconStyle: {paddingLeft: 6},
});

export default observer(ProfileEditButton);
