import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {size16, WINDOW_HEIGHT} from '../../consts/consts.ts';
import store from '../../store/intex.ts';
import {ParamListBase, useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {observer} from 'mobx-react-lite';

const BasketTotalPrice = () => {
  const navigation: NativeStackNavigationProp<ParamListBase> = useNavigation();
  const {totalBasketPrice, onOrderProducts} = store.productsStore;
  return (
    <TouchableOpacity
      onPress={() => {
        onOrderProducts();
        navigation.popToTop();
      }}
      style={style.totalPriceBlock}>
      <Text
        style={style.totalPriceText}>{`Subtotal: ${totalBasketPrice} ₽.`}</Text>
    </TouchableOpacity>
  );
};

const style = StyleSheet.create({
  totalPriceBlock: {
    height: WINDOW_HEIGHT / 12,
    backgroundColor: '#7163ed',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    borderWidth: 1,
    borderColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },

  totalPriceText: {
    textAlign: 'center',
    fontSize: size16,
    color: '#fff',
  },
});

export default observer(BasketTotalPrice);
