import React from 'react';
import {Image, StyleSheet, Text, TextInput, View} from 'react-native';
import {observer} from 'mobx-react-lite';
import store from '../../store/intex.ts';
import {
  imageSize,
  size16,
  size22,
  size44,
  WINDOW_WIDTH,
} from '../../consts/consts.ts';

const ProfileInfo = () => {
  const {orderHistoryList} = store.productsStore;
  const {
    userName,
    temporaryName,
    isEditMode,
    setUserName,
    setEditMode,
    onSaveChanges,
  } = store.profileStore;

  return (
    <View style={style.profileImageContainer}>
      <Image
        style={style.image}
        source={require('../../../assets/images/profile.png')}
        resizeMode={'contain'}
      />
      {isEditMode ? (
        <TextInput
          style={style.nameInput}
          placeholder="Your name"
          value={isEditMode ? temporaryName : userName}
          onChangeText={(text: string) => setUserName(text)}
          onSubmitEditing={() => {
            setEditMode(false);
            onSaveChanges(true);
          }}
        />
      ) : (
        <View style={{height: size44}}>
          <Text numberOfLines={1} ellipsizeMode="tail" style={style.nameText}>
            {userName ? userName : 'Anonymous'}
          </Text>
        </View>
      )}
      {orderHistoryList.length !== 0 && (
        <View style={style.orderTitleContainer}>
          <Text style={style.orderTitle}>Order history:</Text>
        </View>
      )}
    </View>
  );
};

const style = StyleSheet.create({
  nameInput: {
    width: WINDOW_WIDTH / 2,
    height: WINDOW_WIDTH / 8,
    backgroundColor: '#f4f5f7',
    borderRadius: 16,
    paddingLeft: 10,
  },
  nameText: {fontSize: size22, color: '#000', fontWeight: 'bold'},
  orderTitleContainer: {
    width: WINDOW_WIDTH * 0.95,
    paddingTop: 22,
    paddingBottom: 12,
  },
  orderTitle: {
    fontSize: size16,
    color: '#000',
    fontWeight: 'bold',
    paddingLeft: 12,
  },
  profileImageContainer: {alignItems: 'center'},

  image: {
    marginTop: 20,
    height: imageSize,
    width: imageSize,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default observer(ProfileInfo);
