import React, {useEffect} from 'react';
import {observer} from 'mobx-react-lite';
import store from '../store/intex.ts';
import {OrderHistoryList, RootStackParamList} from '../inerfaces/interfaces.ts';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome5';
import {
  NativeStackNavigationProp,
  NativeStackScreenProps,
} from '@react-navigation/native-stack';
import ListEmptyComponent from './components/ListEmptyComponent.tsx';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  imageSize,
  ORDER_HISTORY_SCREEN,
  size14,
  size16,
  size22,
  size44,
  WINDOW_HEIGHT,
  WINDOW_WIDTH,
} from '../consts/consts.ts';
import ProfileInfo from './components/ProfileInfo.tsx';

type profileProps = NativeStackScreenProps<RootStackParamList, 'ProfileScreen'>;
type ProfileScreenNavigationProp = NativeStackNavigationProp<
  RootStackParamList,
  'ProfileScreen'
>;

const ProfileScreen = ({navigation}: profileProps) => {
  const {setEditMode, onSaveChanges} = store.profileStore;
  const {orderHistoryList} = store.productsStore;

  useEffect(() => {
    return () => {
      setEditMode(false);
      onSaveChanges(false);
    };
  }, [navigation, onSaveChanges, setEditMode]);

  return (
    <View style={style.profileContainer}>
      <ProfileInfo />
      <FlatList
        style={style.listContainer}
        contentContainerStyle={style.listContentContainer}
        data={orderHistoryList}
        renderItem={({item}) => renderItem(item, navigation)}
        keyExtractor={item => item.orderId.toString()}
        ListEmptyComponent={renderListEmptyComponent}
      />
    </View>
  );
};

const renderItem = (
  item: OrderHistoryList,
  navigation: ProfileScreenNavigationProp,
) => {
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate(ORDER_HISTORY_SCREEN, {
          products: item.products,
        });
        store.profileStore.setEditMode(false);
        store.profileStore.onSaveChanges(false);
      }}
      style={style.itemContainer}>
      <View style={style.infoBlock}>
        <Text style={style.titleText}>{`Заказ номер ${item.orderId}`}</Text>
        <Text style={style.totalText}>
          Общая сумма заказа:
          <Text style={style.priceText}>{` ${item.totalPrice} ₽.`}</Text>
        </Text>
      </View>
      <View style={style.orderBlock}>
        <IconFontAwesome name={'chevron-right'} size={size16} color="#000" />
      </View>
    </TouchableOpacity>
  );
};

const renderListEmptyComponent = () => {
  return <ListEmptyComponent title={'Order history is empty.'} />;
};

const style = StyleSheet.create({
  profileContainer: {backgroundColor: '#fff', flex: 1, alignItems: 'center'},
  listContainer: {
    height: WINDOW_HEIGHT,
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
  },
  nameInput: {
    width: WINDOW_WIDTH / 2,
    height: size44,
    backgroundColor: '#f4f5f7',
    borderRadius: 16,
    paddingLeft: 10,
  },
  nameText: {fontSize: size22, color: '#000', fontWeight: 'bold'},
  orderTitleContainer: {
    width: WINDOW_WIDTH * 0.95,
    paddingTop: 22,
    paddingBottom: 12,
  },
  orderTitle: {
    fontSize: size16,
    color: '#000',
    fontWeight: 'bold',
    paddingLeft: 12,
  },
  profileImageContainer: {alignItems: 'center'},
  listContentContainer: {
    paddingBottom: 10,
  },
  totalText: {color: '#000', fontSize: size14},
  image: {
    marginTop: 20,
    height: imageSize,
    width: imageSize,
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemContainer: {
    width: WINDOW_WIDTH * 0.9,
    backgroundColor: '#fff',
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 12,
    marginHorizontal: 12,
  },
  infoBlock: {
    marginLeft: 12,
    marginVertical: size22,
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },

  productNumberText: {fontSize: size16, color: '#fff', paddingHorizontal: 10},
  titleText: {fontWeight: 'bold', fontSize: size16, color: '#000'},
  priceText: {
    fontWeight: 'bold',
    fontSize: size14,
    color: '#7464ef',
    paddingTop: 2,
  },
  orderBlock: {paddingRight: 12},
});

export default observer(ProfileScreen);
