import React from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import store from '../store/intex.ts';
import {observer} from 'mobx-react-lite';
import {ProductItem} from '../inerfaces/interfaces.ts';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome5';
import {
  imageSize,
  size12,
  size14,
  size16,
  size22,
  WINDOW_HEIGHT,
} from '../consts/consts.ts';
import ListEmptyComponent from './components/ListEmptyComponent.tsx';
import BasketTotalPrice from './components/BasketTotalPrice.tsx';

const BasketScreen = () => {
  const {basketProductList} = store.productsStore;
  const {totalBasketPrice} = store.productsStore;

  return (
    <View style={style.container}>
      <FlatList
        style={style.listContainer}
        contentContainerStyle={style.contentContainerStyle}
        data={basketProductList}
        renderItem={({item}) => renderItem(item)}
        keyExtractor={item => item.id.toString()}
        ListEmptyComponent={renderListEmptyComponent}
      />
      {totalBasketPrice ? <BasketTotalPrice /> : null}
    </View>
  );
};

const renderItem = (item: ProductItem) => {
  return (
    <View style={style.itemContainer}>
      <View>
        {item.image ? (
          <Image
            style={style.image}
            source={{
              uri: item.image,
            }}
            resizeMode={'contain'}
          />
        ) : (
          <View style={style.image}>
            <Text>No image</Text>
          </View>
        )}
      </View>
      <View style={style.infoBlock}>
        <View>
          <Text style={style.titleText}>{item.title}</Text>
          <Text style={style.priceText}>{`${item.price} ₽.`}</Text>
        </View>
        <View style={style.productNumber}>
          <TouchableOpacity
            hitSlop={{left: size12, right: size12, top: size12, bottom: size12}}
            onPress={() => store.productsStore.removeFromBasket(item)}>
            <IconFontAwesome name={'minus'} size={size16} color="#fff" />
          </TouchableOpacity>
          <Text style={style.productNumberText}>{item.amount}</Text>
          <TouchableOpacity
            hitSlop={{left: size12, right: size12, top: size12, bottom: size12}}
            onPress={() => store.productsStore.addToBasket(item)}>
            <IconFontAwesome name={'plus'} size={size16} color="#fff" />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const renderListEmptyComponent = () => {
  return <ListEmptyComponent title={'Cart is empty.'} />;
};

const style = StyleSheet.create({
  container: {flex: 1},
  listContainer: {
    height: WINDOW_HEIGHT,
    margin: 6,
    flex: 1,
  },
  contentContainerStyle: {
    paddingBottom: WINDOW_HEIGHT / 12,
  },
  itemContainer: {
    backgroundColor: '#fff',
    borderRadius: 12,
    flexDirection: 'row',
    paddingHorizontal: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 12,
    marginHorizontal: 12,
  },
  image: {
    height: imageSize,
    width: imageSize,
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoBlock: {
    marginLeft: 12,
    marginVertical: size22,
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },
  productNumber: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: '#7163ed',
    borderRadius: 20,
    paddingVertical: 8,
    paddingHorizontal: 4,
  },
  productNumberText: {fontSize: size16, color: '#fff', paddingHorizontal: 10},
  titleText: {fontWeight: 'bold', fontSize: size16, color: '#000'},
  priceText: {
    fontWeight: 'bold',
    fontSize: size14,
    color: '#7464ef',
    paddingTop: 2,
  },
});

export default observer(BasketScreen);
