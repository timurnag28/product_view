import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import {
  imageSize,
  size14,
  size16,
  size22,
  WINDOW_HEIGHT,
} from '../consts/consts.ts';
import React from 'react';
import {ProductItem, RootStackParamList} from '../inerfaces/interfaces.ts';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
type OrderHistoryProps = NativeStackScreenProps<
  RootStackParamList,
  'OrderHistoryScreen'
>;
const OrderHistoryScreen = ({route}: OrderHistoryProps) => {
  const {products} = route.params;
  return (
    <FlatList
      style={style.listContainer}
      contentContainerStyle={style.contentListContainer}
      data={products}
      renderItem={({item}) => renderItem(item)}
      keyExtractor={item => item.id.toString()}
    />
  );
};

const renderItem = (item: ProductItem) => {
  return (
    <View style={style.itemContainer}>
      <View>
        {item.image ? (
          <Image
            style={style.image}
            source={{
              uri: item.image,
            }}
            resizeMode={'contain'}
          />
        ) : (
          <View style={style.image}>
            <Text>No image</Text>
          </View>
        )}
      </View>
      <View style={style.infoBlock}>
        <View>
          <Text style={style.titleText}>{item.title}</Text>
          <Text style={style.priceText}>{`${item.price} ₽.`}</Text>
        </View>
        <Text style={style.productNumberText}>{`Qty: ${item.amount}`}</Text>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  listContainer: {
    height: WINDOW_HEIGHT,
    margin: 6,
    flex: 1,
  },
  contentListContainer: {
    paddingBottom: 10,
  },
  itemContainer: {
    backgroundColor: '#fff',
    borderRadius: 12,
    flexDirection: 'row',
    paddingHorizontal: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 12,
    marginHorizontal: 12,
  },
  image: {
    height: imageSize,
    width: imageSize,
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoBlock: {
    marginLeft: 12,
    marginVertical: size22,
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },

  productNumberText: {fontSize: size16, color: '#000'},
  titleText: {fontWeight: 'bold', fontSize: size16, color: '#000'},
  priceText: {
    fontWeight: 'bold',
    fontSize: size14,
    color: '#7464ef',
    paddingTop: 2,
  },
});

export default OrderHistoryScreen;
