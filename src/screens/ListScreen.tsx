import {BackHandler, StyleSheet, TouchableOpacity} from 'react-native';
import {WINDOW_HEIGHT} from '../consts/consts.ts';
import Header from './components/Header.tsx';
import BottomSheet from './components/BottomSheet.tsx';
import {useSharedValue, withSpring, withTiming} from 'react-native-reanimated';
import ProductList from './components/ProductList.tsx';
import {observer} from 'mobx-react-lite';
import store from '../store/intex.ts';
import React, {useEffect} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import {autorun} from 'mobx';

const ListScreen = () => {
  const {setBottomSheetActive, onCancelSearch} = store.productsStore;
  const offset = useSharedValue(0);
  const height = useSharedValue(0);

  const toggleSheet = (value: boolean, isAnimation: boolean = true) => {
    if (value) {
      offset.value = withSpring(0);
      height.value = withTiming(WINDOW_HEIGHT / 4);
      setBottomSheetActive(true);
    } else {
      height.value = isAnimation ? withTiming(0) : 0;
      store.productsStore.setBottomSheetActive(false);
    }
  };

  useFocusEffect(() => {
    onCancelSearch();
    toggleSheet(false);
  });

  useEffect(() =>
    autorun(() => {
      const backAction = () => {
        toggleSheet(false);
        return true;
      };

      const backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        backAction,
      );

      return () => backHandler.remove();
    }),
  );

  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={() => toggleSheet(false)}
      style={style.mainContainer}>
      <Header toggleSheet={toggleSheet} />
      <ProductList toggleSheet={toggleSheet} />
      <BottomSheet toggleSheet={toggleSheet} offset={offset} height={height} />
    </TouchableOpacity>
  );
};

const style = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#f4f5f7',
    flex: 1,
  },
});

export default observer(ListScreen);
